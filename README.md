# makescript
You pipe a text into it and it makes a script with as many lines from the bottom as you want.

I created it for my university degree, when I had to use commands that didn't remember to compile with lex.

This way, I could make a script that did what I needed and didn't had to remember any of those commands.

The way it was ment to be used was: `history | makeScript <number of commands> <name of the script>`